function supportsCrypto () {
  return window.crypto && crypto.subtle && window.TextEncoder;
}

function hash (algo, str) {
  return crypto.subtle.digest(algo, new TextEncoder().encode(str));
}


function hex(buffer) { // buffer is an ArrayBuffer
  return Array.prototype.map.call(new Uint8Array(buffer), x => ('00' + x.toString(16)).slice(-2)).join('');
}

/*
hash('SHA-256', 'Hello').then(hashed => {
  console.log(hashed); // ArrayBuffer
  console.log(hex(hashed)); // 185f8db32271fe25f561a6fc938b2e264306ec304eda518007d1764826381969
  //console.log(encode64(hashed)); // GF+NsyJx/iX1Yab8k4suJkMG7DBO2lGAB9F2SCY4GWk=
});
*/

document.querySelector('#button_1').addEventListener('click', function() {
  let inp = document.querySelector('#input_1').value;
  hash('SHA-256', inp).then(hashed => {
    document.querySelector('#out_1').innerHTML += inp + ': ' + hex(hashed) + '<br>';
  });
});

document.querySelector('#button_2').addEventListener('click', function() {
  let inp = document.querySelector('#input_2').value;
  hash('SHA-256', inp).then(hashed => {
    let correct = document.querySelector('#comp_2').innerHTML == hex(hashed);
    document.querySelector('#out_2').innerHTML = correct + ': ' + hex(hashed);
  });
});

document.querySelector('#button_3').addEventListener('click', function() {
  let inp = document.querySelector('#input_3').value;
  let salt = document.querySelector('#salt_3').value;

  Promise.all([hash('SHA-256', inp), hash('SHA-256', inp + salt)])
    .then(arr => {
      let array = arr.map(x => hex(x));
      document.querySelector('#out_3').innerHTML = 'ungelsalzen: ' + array[0] +'<br>';
      document.querySelector('#out_3').innerHTML += 'gelsalzen: ' + array[1];
    });
});
